export interface Message {
  text: string;
  additionalData?: {
    options?: { name: string; value: boolean; }[];
  };
  isUser?: boolean;
}