import { Course } from './course';

export interface Result {
  id: string;
  courses: Course[];
  conversationId: string;
}