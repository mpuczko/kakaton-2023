import { useRef, useEffect } from 'react';
import { Message } from '../types/message';
import { ChatBubble } from './chat-bubble';

export const ChatStream = ({ messages, className }: { messages: Message[]; className?: string; }) => {
  const chatContainerRef = useRef<HTMLDivElement>(null);
  
  useEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
    }
  }, [messages]);

  return (
    <div className={`flex flex-col w-full max-h-full overflow-scroll pr-5 ${className}`}
    ref={chatContainerRef}
    >
      {messages.map((message, index) => {
        return <ChatBubble key={index} message={message} left={!message.isUser} />;
      })}
    </div>
  );
};