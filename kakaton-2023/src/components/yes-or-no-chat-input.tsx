export const YesOrNoChatInput = ({ onSend }: { onSend: (text: string) => void; }) => {
  return (
    <div className='flex w-full pr-5'>
      <button className='bg-green-500 hover:bg-green-600 text-white rounded-lg px-4 py-2 ml-2 w-full' onClick={() => onSend('Tak')}>
        Tak
      </button>
      <button className='bg-red-500 hover:bg-red-600 text-white rounded-lg px-4 py-2 ml-2 w-full' onClick={() => onSend('Nie')}>
        Nie
      </button>
    </div>
  );
};
