export const FieldOfStudyCard = ({
  schoolName,
  fieldOfStudyName,
  level,
  language,
  disciplines,
  city,
  isStationary,
}: {
  schoolName: string;
  fieldOfStudyName: string;
  level: string;
  language: string;
  disciplines: string[];
  city: string;
  isStationary: boolean;
}) => {
  return (
    <div className='flex flex-col gap-2 border bg-gray-100 border-gray-200 rounded-lg shadow-md w-full md:w-max'>
      <h2 className='text-xl m-4'>{schoolName}</h2>
      <div className='flex flex-col gap-2 p-4 border bg-white border-gray-200 rounded-lg shadow-md'>
        <div className='flex flex-col gap-1'>
          <h2 className='text-xl font-bold'>{fieldOfStudyName}</h2>
          <p className='text-sm'>Stopień: <strong>{level}</strong></p>
          <p className='text-sm'>Język: <strong>{language}</strong></p>
          <p className='text-sm'>Dyscypliny: <strong>{disciplines.join(', ')}</strong></p>
          <p className='text-sm'>Miasto: <strong>{city}</strong></p>
          <p className='text-sm'><strong>{isStationary ? 'stacjonarne' : 'niestacjonarne'}</strong></p>
        </div>
      </div>
    </div>
  );
};
