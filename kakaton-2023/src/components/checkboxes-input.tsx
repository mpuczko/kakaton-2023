import { useState } from 'react';
import { Checkbox } from './checkbox';

export const CheckboxesInput = ({ onSend, options }: {
  onSend: (val: { name: string; value: boolean; }[]) => void;
  options: { name: string; value: boolean; }[];
}) => {
  const [values, setValues] = useState(options);
  const canSend = values.reduce((acc, val) => val.value || acc, false);

  return (
    <div className='flex flex-col w-full pr-5 gap-6'>
      <div
        className='flex flex-wrap gap-2'
      >
      {options.map((option, index) => (
          <Checkbox
            onChange={(val) => {
              const newValues = [...values];
              newValues[index] = { name: option.name, value: val };
              setValues(newValues);
            }}
            value={values[index].value}
            label={option.name}
            key={`checkbox-${index}`}
          />
      ))}
      </div>
      <button className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2 disabled:bg-gray-500 disabled:hover:bg-gray-500'
        onClick={canSend ? () => onSend(values) : undefined}
        disabled={!canSend}
      >
        Wyślij
      </button>

    </div>
  );
};
