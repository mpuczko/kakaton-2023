import { HTMLProps } from 'react';

export const Card = (props: HTMLProps<HTMLDivElement>) => {
  return (
    <div 
    {...props}
    className={`bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 ${props.className}`}
    />
  )
};