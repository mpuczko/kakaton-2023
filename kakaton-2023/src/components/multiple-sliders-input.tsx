import { useState } from 'react';
import { SliderInput } from './slider-input';

export const MultipleSlidersInput = ({ slidersConfig, onSend }: { 
  slidersConfig: { label: string; }[];
  onSend: (values: {label: string; value: string;}[]) => void;
 }) => {
  const [values, setValues] = useState(slidersConfig.map(() => 50));
  const handleChange = (index: number) => (value: number) => {
    const newValues = [...values];
    newValues[index] = value;
    setValues(newValues);
  };

  return (
    <div className='flex flex-col w-full pr-5'>
      {slidersConfig.map((config, index) => (
        <div className='flex flex-col mb-2 mr-2' key={index}>
          <div className='text-gray-600'>{config.label}</div>
          <SliderInput value={values[index]} onChange={handleChange(index)} />
        </div>
      ))}
      <button className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2'
        onClick={() => onSend(slidersConfig.map((config, index) => ({ label: config.label, value: values[index].toString() })))}
      >
        Wyślij
      </button>
    </div>
  );
};
