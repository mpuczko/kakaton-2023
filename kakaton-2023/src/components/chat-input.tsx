import { FormEvent, useState } from 'react';

export const ChatInput = ({ className, onSubmit }: { className?: string; onSubmit: (text: string) => void; }) => {
  const [text, setText] = useState('');
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (text === '') return;
    onSubmit(text);
    setText('');
  };
  return (
    <form className={`flex w-full px-5 ${className}`} onSubmit={handleSubmit}>
      <input
        className='flex-1 rounded-lg bg-gray-100 px-4 py-2'
        type='text'
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <input className='bg-blue-500 hover:bg-blue-600 disabled:bg-gray-500 disabled:hover:bg-gray-500 text-white rounded-lg px-4 py-2 ml-2'
        disabled={text === ''}
        type='submit'
        value={'Wyślij'}
      />
    </form>
  );
};
