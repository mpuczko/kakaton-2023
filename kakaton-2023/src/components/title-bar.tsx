import { useContext } from 'react';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { UserContext } from '../contexts/user-context';

const StyledLink = ({ to, className, text }: {
  to: string;
  className?: string;
  text: string
}) => {
  const location = useLocation();

  return <Link to={to} className={`text-gray-500 hover:text-gray-800 p-3 ${location.pathname === to ? 'font-bold' : ''} ${className}`}>
    {text}
  </Link>
};

export const TitleBar = () => {
  const { isLoggedIn, email, setEmail } = useContext(UserContext);
  
  return (
    <div className='flex justify-between mb-8 border-solid p-x-3 border-b-2 border-gray-100'>
      <div className='flex'>
        <StyledLink to='/' text='Strona główna' />
        {isLoggedIn && <StyledLink to='/results' text='Wyniki wyszukiwań' />}
      </div>
      <div className='flex'>
        {!isLoggedIn && <StyledLink to='/login' text='Zaloguj się' />}
        {!isLoggedIn && <StyledLink to='/register' text='Zarejestruj się' />}
        {isLoggedIn && <p className='p-3 text-gray-500'>Zalogowano jako {email}</p>}
        {isLoggedIn && <button className={`text-gray-500 hover:text-gray-800 p-3`} onClick={() => setEmail('')}>Wyloguj się</button>}
      </div>
    </div>
  );
};
