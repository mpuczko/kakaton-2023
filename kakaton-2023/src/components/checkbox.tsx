export const Checkbox = ({ onChange, value, label }: {
  onChange: (val: boolean) => void;
  value: boolean;
  label: string;
}) => {
  return (
    <button
      className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2 flex flex-row gap-2 justify-start items-center flex-initial'
      onClick={() => onChange(!value)}
      style={{
        // hack
        width: 'calc(50% - 4px)'
      }}
    >
      <label className="checkBox">
        <input type="checkbox"
          onClick={(e) => e.stopPropagation()}
          checked={value}
        />
        <div className="transition"></div>
      </label>
      <label>{label}</label>
    </button>
  );
};
