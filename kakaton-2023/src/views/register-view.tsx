import { useContext, useState } from 'react';
import { useNavigate } from 'react-router';
import { Card } from '../components/card';
import { UserContext } from '../contexts/user-context';

export const RegisterView = () => {
  const { setEmail } = useContext(UserContext);
  const navigate = useNavigate();

  const [em, setEm] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const email = em;
    setEmail(email);
    navigate('/');
  };
  
  return (
    <div className='flex flex-col items-center text-black'>
      <h2 className='text-4xl'>Rejestracja</h2>
      <Card className='container sm:w-80 mt-8'>
        <form onSubmit={handleSubmit}>
          <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='email'>
            Adres e-mail
          </label>

          <input
            className='shadow appearance-none border rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            id='email'
            type='email'
            placeholder='Adres e-mail'
            onChange={(e) => setEm(e.target.value)}
            value={em}
          />

          <label className='block text-gray-700 text-sm font-bold mb-2' htmlFor='password'>
            Hasło
          </label>

          <input
            className='shadow appearance-none border rounded mb-4 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
            id='password'
            type='password'
            placeholder='******************'
          />

          <input
            className='mb-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
            type='submit'
            value='Zarejestruj się'
          />

        </form>
      </Card>
    </div>
  )
};