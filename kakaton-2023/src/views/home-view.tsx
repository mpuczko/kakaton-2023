import { useNavigate } from 'react-router';
import { createConversation } from '../services/conversation-service';

export const HomeView = () => {
  const navigate = useNavigate();
  
  const onChatStart = async () => {
    const { id: conversationId } = await createConversation();
    navigate(`/conversation/${conversationId}`);
  }

  return (
    <div className='flex flex-col items-center justify-center h-full'>
      <div className='flex flex-col items-center justify-center w-full max-w-md'>
        <h1 className='text-3xl font-bold text-center mb-5'>Dobierz kierunek studiów</h1>
        <p className='text-center mb-5'>
          Napisz do nas, a my dobierzemy dla Ciebie najlepszy kierunek studiów na podstawie Twoich zainteresowań.
        </p>
        <button className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2 w-full'
          onClick={onChatStart}
        >
          Rozpocznij
        </button>
      </div>
    </div>
  );
};
