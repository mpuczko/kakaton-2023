import { useState } from 'react';
import { ChatStream } from '../components/chat-stream';
import { FieldOfStudyCard } from '../components/field-of-study-card';
import { SliderInput } from '../components/slider-input';

const mockPriorities = ['fizyka', 'informatyka', 'matematyka', 'angielski'];

export const ResultView = () => {
  const [prioritiesWithValues, setPrioritiesWithValues] = useState<{ priority: string; value: number; }[]>(mockPriorities.map((priority) => ({ priority, value: 100 / mockPriorities.length, }))); // [priority, value

  // sum of all priorities should be 100, it should lower the value of other priorities if wants to increase one. It should not change order
  const onChange = (value: number, priority: string) => {
    const newPriorities = [...prioritiesWithValues];
    const otherPriorities = newPriorities.filter((p) => p.priority !== priority);
    const sumOfOtherPriorities = otherPriorities.reduce((acc, curr) => acc + curr.value, 0);
    const newOtherPriorities = otherPriorities.map((p) => ({ ...p, value: p.value - ((p.value / sumOfOtherPriorities) * (value - newPriorities.find((p) => p.priority === priority)?.value)) }));
    const newPriority = { ...newPriorities.find((p) => p.priority === priority)!, value };
    const newPriorityIndex = newPriorities.findIndex((p) => p.priority === priority);

    newOtherPriorities.splice(newPriorityIndex, 0, newPriority);
    setPrioritiesWithValues([...newOtherPriorities]);
  };

  return <div>
  <h2 className='text-4xl mb-4 text-center'>Znalezione kierunki</h2>
  <div>
    <h3 className='ml-8 mb-4'><strong>Priorytety</strong></h3>
    <div className='flex flex-wrap gap-2 mb-10 justify-around'>
      {prioritiesWithValues.map((priority, index) => (
        <div className='flex flex-col w-full md:w-5/12' key={`slider-${index}`}>
          <div className='text-gray-600'>{priority.priority}</div>
          <SliderInput value={priority.value} onChange={(value) => onChange(value, priority.priority)} />
        </div>
      ))}
      </div>
  </div>
  <div className='flex gap-4 flex-wrap lg justify-center mb-8'>
    <FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    /><FieldOfStudyCard
      schoolName='Politechnika Warszawska'
      fieldOfStudyName='Informatyka'
      level='I stopnia'
      language='polski'
      disciplines={['informatyka', 'matematyka']}
      city='Warszawa'
      isStationary={true}
    />
    </div>
    <h2 className='text-2xl mb-4 text-center'>Historia konwersacji</h2>
    <ChatStream messages={[]} className='h-full' />
  </div>;
};
