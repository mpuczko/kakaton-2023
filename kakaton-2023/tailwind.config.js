/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      height: {
        "screen-80": "80vh",
        "screen-90": "90vh",
        "screen-95": "95vh",
      },
      minWidth: {
        "1/4": "25%",
        "5/12": "45%",
        "1/2": "50%",
        "3/4": "75%",
      },
      maxWidth: {
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
      },
    },
  },
  plugins: [],
}

